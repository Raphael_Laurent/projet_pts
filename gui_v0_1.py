import tkinter as tk
from tkinter import messagebox
import matplotlib
import matplotlib.pyplot as plt 
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
matplotlib.use("TkAgg")
from code_v0_3 import *
import time
import random


class Inputs(object):
	def __init__(self):
		self.nb_generations = 100
		self.nb_enfants = 100
		self.nb_individus = self.nb_enfants
		self.percent_best_individus_gardes = 5
		self.nb_pivots = 1
		self.percent_parent = 30
		self.percent_mutation = 20
		self.percent_injection = 10
		self.nb_injection = int((self.percent_injection / 100) * self.nb_individus)
		self.nb_best_individus_gardes = int((self.percent_best_individus_gardes / 100) * self.nb_individus)
		self.nb_parents = int((self.percent_parent / 100) * self.nb_individus)
		self.nb_villes = 20
		self.list_villes_for_run = random.sample(villes, self.nb_villes)

Inputs = Inputs()

class Outputs(object):
	def __init__(self):
		self.list_generations = []
		self.list_fit_by_generation = []
Outputs = Outputs()

#verifie les inputs rentrés par l'utilisateur lors de l'appuis sur la validation
def validate_inputs():
	try:
		nb_g = int(entry_nb_generations.get())
		nb_e = int(entry_nb_enfants.get())
		nb_p = int(entry_nb_pivots.get())
		per_parents = int(entry_percent_parent.get())
		per_mutation = int(entry_percent_mutation.get())
		per_best = int(entry_percent_best.get())
		per_inj = int(entry_percent_injection.get())
		if nb_g < 1 :
			messagebox.showinfo("Inputs error", "Là ça va pas.. Veuillez entrer un nombre de générations positif")
			return
		if nb_e < 1 :
			messagebox.showinfo("Inputs error", "Là ça va pas.. Veuillez entrer un nombre d'individus positif")
			return
		if nb_p < 1 :
			messagebox.showinfo("Inputs error", "Là ça va pas.. Veuillez entrer un nombre de pivots positif \n (1 ou 2 conseillé)")
			return

	except:
		messagebox.showinfo("Inputs error", "Là ça va pas.. Veuillez entrer un entier")
		return
	Inputs.percent_parent = per_parents
	Inputs.percent_mutation = per_mutation
	Inputs.percent_best_individus_gardes = per_best
	Inputs.nb_generations = nb_g
	Inputs.nb_enfants = nb_e
	Inputs.nb_individus = Inputs.nb_enfants
	Inputs.nb_pivots = nb_p
	Inputs.percent_injection = per_inj
	Inputs.nb_injection = int((Inputs.percent_injection / 100) * Inputs.nb_individus)
	Inputs.nb_parents = int((Inputs.percent_parent / 100) * Inputs.nb_individus)
	Inputs.nb_best_individus_gardes = int((Inputs.percent_best_individus_gardes / 100) * Inputs.nb_individus)
	

	label_nb_parent.config(text = "# parents: \n" + str(Inputs.nb_parents))
	label_nb_best.config(text = "# of bests to keep: \n" + str(Inputs.nb_best_individus_gardes))
	label_nb_injection.config(text = "# Injection: \n" + str(Inputs.nb_injection))
	label_nb_individus.config(text = "# Individus: \n" + str(Inputs.nb_individus))

	window.update_idletasks()

def validate_villes_input():
	nb_v = int(entry_nb_ville.get())
	Inputs.nb_villes = nb_v

	Inputs.list_villes_for_run = random.sample(villes, Inputs.nb_villes)
	x_list = [ville.x for ville in Inputs.list_villes_for_run]
	x_list.append(x_list[0])
	y_list = [ville.y for ville in Inputs.list_villes_for_run]
	y_list.append(y_list[0])

	ax.clear()
	ax.scatter(x_list, y_list, color = 'r')
	ax.plot(x_list, y_list)
	canvas.draw()

	window.update_idletasks()


def run():
	generation = Generation.randomGen(Inputs.nb_enfants, Inputs.list_villes_for_run)
	monde_x_list = []
	monde_y_list = []
	monde_fit = 999999
	Outputs.list_generations = []
	Outputs.list_fit_by_generation = []
	for i in range (1, Inputs.nb_generations + 1):
		label_nb_generation.config(text = "Generation: " + "\n" + str(i))
		#nouvelle géneration ;
		generation = generation.pivotMultipleConversation(Inputs.nb_best_individus_gardes, Inputs.nb_parents, Inputs.nb_enfants - Inputs.nb_injection,Inputs.nb_pivots, Inputs.list_villes_for_run)
		#mutation :
		for indi in generation.pop :
			indi.mutation(Inputs.percent_mutation)
		#injection:
		generation.injection(Inputs.nb_injection, Inputs.list_villes_for_run)
		Inputs.nb_individus = len(generation.pop)
		label_nb_individus.config(text = "# individus: \n" + str(Inputs.nb_individus))

		generation.findCout()
		label_generation_fit.config(text = "Generation, best fit : "+ "\n" + str(round(generation.meilleurCout, 2)))
		Outputs.list_generations.append(i)
		Outputs.list_fit_by_generation.append(generation.meilleurCout)

		if monde_fit > generation.meilleurCout :
			monde_fit = generation.meilleurCout
			monde_x_list = [ville.x for ville in generation.meilleurIndi.trajet]
			monde_x_list.append(monde_x_list[0])
			monde_y_list = [ville.y for ville in generation.meilleurIndi.trajet]
			monde_y_list.append(monde_y_list[0])
			label_monde_fit.config(text = "Run, best fit : "+ "\n" + str(round(monde_fit, 2)))

		x_list = [ville.x for ville in generation.meilleurIndi.trajet]
		x_list.append(x_list[0])
		y_list = [ville.y for ville in generation.meilleurIndi.trajet]
		y_list.append(y_list[0])
		ax.clear()
		ax.scatter(x_list, y_list, color = 'r')
		ax.plot(x_list, y_list)
		ax.plot(monde_x_list, monde_y_list, color = 'g', linestyle = 'dashed', linewidth = 1)
		canvas.draw()

		window.update_idletasks()
		#time.sleep(0.1)
	ax.clear()
	ax.scatter(x_list, y_list, color = 'r')
	ax.plot(monde_x_list, monde_y_list, color = 'g', linewidth = 2)
	canvas.draw()


	window_fit = tk.Toplevel(window)
	window_fit.title("Projet PTS | Fitness")
	fit_graph = plt.Figure(figsize = (10, 5), dpi = 100)
	ax_fit = fit_graph.add_subplot(111)
	ax_fit.plot(Outputs.list_generations, Outputs.list_fit_by_generation)
	canvas_fit = FigureCanvasTkAgg(fit_graph, window_fit)
	canvas_fit.draw()
	canvas_fit.get_tk_widget().pack(side = tk.BOTTOM, fill = tk.BOTH, expand = True)
	canvas_fit._tkcanvas.pack(side = tk.TOP, fill = tk.BOTH, expand = True)



window = tk.Tk()
#window.wm_state('zoomed')
window.title("Projet PTS | Genetic Algorithm")
#window.minsize(1280, 720)

widgets_menu = tk.Frame(window, bg = "blue", height = 850, width = 200, padx = 5, pady = 5)
widgets_menu.pack_propagate(False)
widgets_menu.pack(side = tk.LEFT)

stats_menu = tk.Frame(window, bg = "red", height = 80, width = 1280, padx = 5, pady = 5)
stats_menu.pack_propagate(False)
stats_menu.pack(side = tk.BOTTOM)

label = tk.Label(window, text = "Genetic Algorithm")
label.pack(pady = 5, padx = 5)

#STATS MENU ------------------------------------
label_nb_generation = tk.Label(stats_menu, text = "Generation: " + "\n", bg = "orange", padx = 10, pady = 5, borderwidth = 4, relief="groove")
label_nb_generation.pack(side = tk.LEFT, )

label_generation_fit = tk.Label(stats_menu, text = "Generation, best fit : "+ "\n", bg = "orange", padx = 10, pady = 5, borderwidth = 4, relief="groove")
label_generation_fit.pack(side = tk.LEFT)

label_monde_fit = tk.Label(stats_menu, text = "Run, best fit : "+ "\n", bg = "orange", padx = 10, pady = 5, borderwidth = 4, relief="groove")
label_monde_fit.pack(side = tk.LEFT)

#__________________________________________________________________
frame_labels = tk.Frame(stats_menu, relief="groove", borderwidth = 4)
frame_labels.pack(padx=5, pady=5, side = tk.LEFT)

label_nb_individus = tk.Label(frame_labels, text = "# Individus: \n" + str(Inputs.nb_individus), relief="groove", borderwidth = 3)
label_nb_individus.pack(padx = 5, pady = 5, side = tk.LEFT)

label_nb_parent = tk.Label(frame_labels, text = "# parents: \n" + str(Inputs.nb_parents), relief="groove", borderwidth = 3)
label_nb_parent.pack(padx = 5, pady = 5, side = tk.LEFT)

label_nb_best = tk.Label(frame_labels, text = "# of bests to keep: \n" + str(Inputs.nb_best_individus_gardes), relief="groove", borderwidth = 3)
label_nb_best.pack(padx = 5, pady = 5, side = tk.LEFT)

label_nb_injection = tk.Label(frame_labels, text = "# injection: \n" + str(Inputs.nb_injection), relief="groove", borderwidth = 3)
label_nb_injection.pack(padx = 5, pady = 5, side = tk.LEFT)
button_quit = tk.Button(stats_menu, text = "Quit", command = window.destroy, width = 6)
button_quit.pack(side = tk.RIGHT)

#WIDGET MENU-----------------------------------------------------
frame_entrys = tk.Frame(widgets_menu, relief="groove", borderwidth = 4)
frame_entrys.pack(padx=5, pady=5, side = tk.TOP)

frame_select_villes = tk.Frame(widgets_menu, relief="groove", borderwidth = 4)
frame_select_villes.pack(padx=5, pady=5, side = tk.TOP)

label_entry_percent_parent = tk.Label(frame_entrys, text = "Percent parents (bests)")
label_entry_percent_parent.pack(padx = 5, side = tk.TOP)
entry_percent_parent = tk.Entry(frame_entrys)
entry_percent_parent.pack(padx = 5, side = tk.TOP)
entry_percent_parent.insert(0, str(Inputs.percent_parent))

label_entry_nb_generation = tk.Label(frame_entrys, text = "# Generations")
label_entry_nb_generation.pack(padx = 5, side = tk.TOP)
entry_nb_generations = tk.Entry(frame_entrys)
entry_nb_generations.pack(padx = 5, side = tk.TOP)
entry_nb_generations.insert(0, str(Inputs.nb_generations))

label_entry_nb_enfants= tk.Label(frame_entrys, text = "# enfants / # individus at start")
label_entry_nb_enfants.pack(padx = 5, side = tk.TOP)
entry_nb_enfants = tk.Entry(frame_entrys)
entry_nb_enfants.pack(padx = 5, side = tk.TOP)
entry_nb_enfants.insert(0, str(Inputs.nb_enfants))

label_entry_percent_best = tk.Label(frame_entrys, text = "Percent of bests to keep")
label_entry_percent_best.pack(padx = 5, side = tk.TOP)
entry_percent_best = tk.Entry(frame_entrys)
entry_percent_best.pack(padx = 5, side = tk.TOP)
entry_percent_best.insert(0, str(Inputs.percent_best_individus_gardes))

label_entry_nb_pivots = tk.Label(frame_entrys, text = "# Pivots")
label_entry_nb_pivots.pack(padx = 5, side = tk.TOP)
entry_nb_pivots = tk.Entry(frame_entrys)
entry_nb_pivots.pack(padx = 5, side = tk.TOP)
entry_nb_pivots.insert(0, str(Inputs.nb_pivots))

label_entry_percent_mutation = tk.Label(frame_entrys, text = "Percent mutation")
label_entry_percent_mutation.pack(padx = 5, side = tk.TOP)
entry_percent_mutation = tk.Entry(frame_entrys)
entry_percent_mutation.pack(padx = 5, side = tk.TOP)
entry_percent_mutation.insert(0, str(Inputs.percent_mutation))

label_entry_percent_injection = tk.Label(frame_entrys, text = "Percent of injection")
label_entry_percent_injection.pack(padx = 5, side = tk.TOP)
entry_percent_injection = tk.Entry(frame_entrys)
entry_percent_injection.pack(padx = 5, side = tk.TOP)
entry_percent_injection.insert(0, str(Inputs.percent_injection))

button_send_entrys = tk.Button(frame_entrys, text = "Validate Inputs", command = validate_inputs)
button_send_entrys.pack(pady = 5)

label_entry_nb_villes = tk.Label(frame_select_villes, text = "Nb Citys")
label_entry_nb_villes.pack(padx = 5, side = tk.TOP)
entry_nb_ville = tk.Entry(frame_select_villes)
entry_nb_ville.pack(padx = 5, side = tk.TOP)
entry_nb_ville.insert(0, str(Inputs.nb_villes))

button_send_nb_villes = tk.Button(frame_select_villes, text = "Validate Nb Citys", command = validate_villes_input)
button_send_nb_villes.pack(pady = 5)

button_run = tk.Button(widgets_menu, text = "RUN Algorithm", bg = "cyan", padx = 5, pady = 5, command = run)
button_run.pack(side = tk.BOTTOM)
#____________________________________________________________________


#Initialisation :
x_list = [ville.x for ville in Inputs.list_villes_for_run]
x_list.append(x_list[0])
y_list = [ville.y for ville in Inputs.list_villes_for_run]
y_list.append(y_list[0])

GA_graph = plt.Figure(figsize = (5,5), dpi = 100)
ax = GA_graph.add_subplot(111)
ax.scatter(x_list, y_list, color = 'r')
ax.plot(x_list, y_list)
canvas = FigureCanvasTkAgg(GA_graph, window)
canvas.draw()
canvas.get_tk_widget().pack(side = tk.BOTTOM, fill = tk.BOTH, expand = True)
canvas._tkcanvas.pack(side = tk.TOP, fill = tk.BOTH, expand = True)

window.mainloop()
