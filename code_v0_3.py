# -*- coding: utf-8 -*-
import math
import random
from random import randrange
from random import randint
from math import pow
import pandas as pd


class Individu(object):
    def __init__(self):
        self.trajet = []
        self.cout = None

    def calculCout(self):
        coutTotal = 0
        for i in range (len(self.trajet) - 2):
            coutTotal += Gene.distance(self.trajet[i + 1], self.trajet[i])#Calcul la distance entre chaque point
        coutTotal += Gene.distance(self.trajet[len(self.trajet)-1], self.trajet[0]) #Ajoute la distance entre le dernier et le premier point
        self.cout = coutTotal

    def addVille(self,ville) :
        self.trajet.append(ville)

        #Enleve les doublons et rajoute les gènes non utilisés
    def verifGen(self, villes):
        tailleIn = len(self.trajet)
        numbNotIn =[]
        numbDup = []
        for i in villes :
            if i not in self.trajet :
                numbNotIn.append(i)
        for i in range(tailleIn) :
            elem = self.trajet[i]
            if(elem in numbDup) :
                continue
            for j in range(i+1,tailleIn) :
                if elem == self.trajet[j] :
                    numbDup.append(elem)
        while len(numbDup) != 0 :
            pos = self.trajet.index(numbDup[-1])
            self.trajet[pos] = numbNotIn.pop()
            numbDup.pop()
            
    def mutation(self,pourcentage) :
        prob = randint(0,100)
        #print("pourcentage : "+str(prob) )
        if prob < pourcentage :
            lenIndi = len(self.trajet)
            pos1 = randint(0,lenIndi-1)
            pos2 = randint(0,lenIndi-1)
            tmp = self.trajet[pos1]
            self.trajet[pos1] = self.trajet[pos2]
            self.trajet[pos2] = tmp
            self.calculCout()
        #Aucune mutation effectuee
        else :
        	return

        
    
    def __str__(self) :
        s = ""
        for ville in self.trajet :
            s += ville.nom+" "
        if self.cout:
            s += "\n"+ str(self.cout) + "\n"
        return s
    
class Generation(object):

    id = 1
    
    def __init__(self):
        self.id = id
        self.pop = []
        self.meilleurCout = None
        self.meilleurIndi = None

        Generation.id += 1

    def randomGen(taille,villes) :
        newGen = Generation()
        for i in range(taille) :
            newGen.pop.append(generateRandomIndiFromVille(villes))
        newGen.findCout()
        return newGen


    def __str__(self) :
        return "Generation, fit : "+ str(self.meilleurCout) +"\n"

    def findCout(self) :
        self.meilleurCout = 99999999
        for trajet in self.pop :
            trajet.calculCout()
            if trajet.cout < self.meilleurCout :
                self.meilleurCout = trajet.cout
                self.meilleurIndi = trajet
    

    def getBestPop(self,nbPop) :
        tmpList = self.pop.copy()
        bestIndi = []
        for i in range(nbPop) :
            tmpMin = 9999999
            indiMin= None

            for indi in tmpList :
                if indi.cout < tmpMin :
                    tmpMin = indi.cout
                    indiMin = indi
            
            bestIndi.append(indiMin)
            tmpList.remove(indiMin)
        return bestIndi

    def best(self) :
        print("5 meilleurs : ")
        bestIndi = self.getBestPop(5)
        for indi in bestIndi :
            print(indi)

    def printAll(self) :
        print("Pleine generation")
        for indi in self.pop :
            print(indi)

    def injection(self,nbIndi,villes) :
        for i in range(nbIndi) :
            newIndi = generateRandomIndiFromVille(villes)
            newIndi.calculCout()
            self.pop.append(newIndi)

    def pivotMultipleConversation(self,nbIndividusGarde,nbIndividusParents,nbEnfants,nbPivot,villes) :
        newGen = Generation()
        
        #Conservation des meilleurs
        indiGarde = self.getBestPop(nbIndividusGarde)
        for indi in indiGarde :
            newGen.pop.append(indi)
            
        #Reproduction
        parents = self.getBestPop(nbIndividusParents)
        tailleIndividu = len(self.pop[0].trajet)
        #Ecart entre chaque parent
        pasPivot = tailleIndividu/(nbPivot+1)
        for i in range(nbEnfants - len(indiGarde)) :
            parent1 = parents[randrange(len(parents))]
            parent2 = parents[randrange(len(parents))]
            indi = Individu()
            #Modulo 2 pour passer du parent 1 au 2
            par = 1
            for i in range (nbPivot+1) :
                #On selectionne le parent
                if par == 1 :
                    parent = parent1
                else :
                    parent = parent2
                for j in range(int(i*pasPivot),int((i+1)*pasPivot),1) :
                    indi.addVille(parent.trajet[j])
                #individu = individu + parent.trajet[ int(i*pasPivot) : int((i+1)*pasPivot) ]
                #On passe au parent suivant
                par = (par+1)%2
            indi.verifGen(villes)
            newGen.pop.append(indi)

        #Calcul du cout et retour
        newGen.findCout()
        return newGen
        
    
            
class Gene(object):
    
    def __init__(self,nom, x, y):
        self.nom = nom
        self.x = x
        self.y = y

    def __str__(self):
        return self.nom + " : "+str(self.x)+";"+str(self.y) 
    
    def distance(geneA,geneB):
        xA = geneA.x
        yA = geneA.y
        xB = geneB.x
        yB = geneB.y
        return (math.sqrt(pow(xB - xA,2) + pow(yB - yA,2))) #Formule de la distance entre 2 points

    def __eq__(self, other): 
        if not isinstance(other, Gene):
            return NotImplemented
        return self.nom == other.nom
        

# fonction pour échanger deux éléments d'index a et b dans une liste
def permutation(liste,premierElement,deuxiemeElement):
    tmp = liste[premierElement]
    liste[premierElement] = liste[deuxiemeElement]
    liste[deuxiemeElement] = tmp
    return liste


#Mutation avec une probabilité de 1/longueurDeLindividu
def evolutionMutation(generation, tailleGenGlobale):
    tailleIndividu = len(generation[0])
    newGen = []
    for i in range(tailleGenGlobale):
        parent = generation[i]
        mutation1 = randint(0,(tailleIndividu - 1))
        mutation2 = randint(0,(tailleIndividu - 1))
        while (mutation1 == mutation2):
            mutation2 = randint(0,(tailleIndividu - 1))
        print('parent',i+1,parent)
        print('position des genes echangés :',mutation1,'et',mutation2)
        individu = permutation(parent,mutation1,mutation2)
        print('individu nouvele gen :',individu)
        newGen.append(individu)
    print('\n[Nouvelle génération] \n')
    return newGen
        
#Genere un individu aléatoire à partir des villes
#Utiliser pour l'injection d'individus aléatoires dans la population lors d'une nouvelle génération
def generateRandomIndiFromVille(villes):
    copyVille = villes.copy()
    random.shuffle(copyVille)
    indi = Individu()
    for ville in copyVille :
        indi.addVille(ville)
    return indi



df_villes_du_monde = pd.read_csv("worldcities.csv")

villes = []
for row in df_villes_du_monde.itertuples(index = True):
	ville = Gene(getattr(row, "city_ascii"), getattr(row, "lng"), getattr(row, "lat"))
	villes.append(ville)



"""Paris = Gene("Paris",48.866667,2.333333)
Marseille = Gene("Marseille",43.300000,5.400000)
Lyon = Gene("Lyon",45.750000,4.850000)
Toulouse = Gene("Toulouse",43.600000, 1.433333)
Nice = Gene("Nice",43.700936,7.268391)
Nantes = Gene("Nantes",47.218637,-1.554136)
Montpellier = Gene("Montpellier",43.5985,3.89687)
Strasbourg = Gene("Strasbourg",48.583333, 7.750000)
Bordeaux = Gene("Bordeaux",44.841225 ,-0.580036)
Lille = Gene("Lille",50.633333, 3.066667)
Brest = Gene("Brest",48.400000,-4.483333)
Orleans = Gene("Orleans",47.916667,1.900000)
Clermont = Gene("Clermont", 45.783333, 3.083333)
Montargis = Gene("Montargis", 47.997290, 2.736291)
Poitiers = Gene("Poitiers", 46.580224, 0.340375)
Bayonne = Gene("Bayonne", 43.492949, -1.474841)
Perpignan = Gene("Perpignan", 42.6886591, 2.8948332)
Dijon = Gene("Dijon", 47.322047, 5.04148)
Cherbourg = Gene("Cherbourg", 49.633731, -1.622137)
Laval = Gene("Laval", 48.0785146, -0.7669906)
Limoges = Gene("Limoges", 45.833619, 1.261105)

villes = []
villes.append(Paris)
villes.append(Marseille)
villes.append(Lyon)
villes.append(Toulouse)
villes.append(Nice)
villes.append(Nantes)
villes.append(Montpellier)
villes.append(Strasbourg)
villes.append(Bordeaux)
villes.append(Lille)
villes.append(Brest)
villes.append(Orleans)
villes.append(Clermont)
villes.append(Montargis)
villes.append(Poitiers)
villes.append(Bayonne)
villes.append(Perpignan)
villes.append(Dijon)
villes.append(Cherbourg)
villes.append(Laval)
villes.append(Limoges)"""

"""
#Test GetBest
gen = Generation.randomGen(10,villes)
#gen.printAll()
gen.printAll()
print("Best")
gen.best()


#Test Mutation
print("Test mutation")
indi = generateRandomIndiFromVille(villes)
indi.calculCout()
print(indi)
print("Si prob < 30, Mutation effective")
indi.mutation(30)
print(indi)

#Test Injection
print("Test Injection")
gen = Generation.randomGen(5,villes)
gen.printAll()
print("Ajout de 2 villes")
gen.injection(2,villes)
gen.printAll()


#Test reproduction + conservation
gen = Generation.randomGen(7,villes)
gen.printAll()

newGen = gen.pivotMultipleConversation(2,4,3,4,villes)
newGen.printAll()
"""

"""gen = generation.randomGen(100,villes)
print(gen)
gen2 = pivotMultiple(gen.pop,100,2)
print(gen2.pop[0].__str__())
verifGen(gen2.pop[0],villes)
print(gen2.pop[0].__str__())
gen2.findCout()
print(gen2)
gen3 = pivotMultiple(gen.pop,100,2)
print(gen3.pop[0].__str__())
verifGen(gen3.pop[0],villes)
print(gen3.pop[0].__str__())
gen3.findCout()
print(gen3)"""




