# -*- coding: utf-8 -*-
from code_v0_3 import *
import copy
import matplotlib.pyplot as plt
import random

#Petit Set de Villes

Paris = Gene("Paris",48.866667,2.333333)
Marseille = Gene("Marseille",43.300000,5.400000)
Lyon = Gene("Lyon",45.750000,4.850000)
Toulouse = Gene("Toulouse",43.600000, 1.433333)
Nice = Gene("Nice",43.700936,7.268391)
Nantes = Gene("Nantes",47.218637,-1.554136)
Montpellier = Gene("Montpellier",43.5985,3.89687)
Strasbourg = Gene("Strasbourg",48.583333, 7.750000)
Bordeaux = Gene("Bordeaux",44.841225 ,-0.580036)
Lille = Gene("Lille",50.633333, 3.066667)
Brest = Gene("Brest",48.400000,-4.483333)
Orleans = Gene("Orleans",47.916667,1.900000)
Clermont = Gene("Clermont", 45.783333, 3.083333)
Montargis = Gene("Montargis", 47.997290, 2.736291)
Poitiers = Gene("Poitiers", 46.580224, 0.340375)
Bayonne = Gene("Bayonne", 43.492949, -1.474841)
Perpignan = Gene("Perpignan", 42.6886591, 2.8948332)
Dijon = Gene("Dijon", 47.322047, 5.04148)
Cherbourg = Gene("Cherbourg", 49.633731, -1.622137)
Laval = Gene("Laval", 48.0785146, -0.7669906)
Limoges = Gene("Limoges", 45.833619, 1.261105)

villes = []
villes.append(Paris)
villes.append(Marseille)
villes.append(Lyon)
villes.append(Toulouse)
villes.append(Nice)
villes.append(Nantes)
villes.append(Montpellier)
villes.append(Strasbourg)
villes.append(Bordeaux)
villes.append(Lille)
villes.append(Brest)
villes.append(Orleans)
villes.append(Clermont)
villes.append(Montargis)

villes.append(Poitiers)
villes.append(Bayonne)
villes.append(Perpignan)
villes.append(Dijon)
villes.append(Cherbourg)
villes.append(Laval)
villes.append(Limoges)

#Gen Initial
res = []
generation = Generation.randomGen(100, villes)
print("Initial Best : "+str(generation.meilleurCout))
genSave = copy.deepcopy(generation)
'''
#LancementPersonnalisé
for i in range (3) :
    generation = copy.deepcopy(genSave)
    generation.findCout()
    print("Best Ini : "+str(generation.meilleurCout))
    meilleurCout = 9999999
    resSim = []
    bestIndividusGarde = int(input("nbIndiGarde "))
    nbParents = int(input("nbParents"))
    nb_Injection = int(input("nbInjection"))
    pourcent_mutation = int(input("%Mut"))
    nbEnfants = len(generation.pop)- nb_Injection
    nbPivot = int(input("nbPivot"))
    '''
'''
    resSim.append(bestIndividusGarde)
    resSim.append(nbParents)
    resSim.append(nb_Injection)
    resSim.append(pourcent_mutation)
    resSim.append(nbEnfants)
    resSim.append(nbPivot)
    
    for i in range(100) :
        generation = generation.pivotMultipleConversation(bestIndividusGarde,nbParents,nbEnfants,nbPivot,villes)
        for indi in generation.pop :
            indi.mutation(pourcent_mutation)
        generation.injection(nb_Injection,villes)
        generation.findCout()
        if generation.meilleurCout < meilleurCout :
            meilleurCout = generation.meilleurCout
        resSim.append(generation.meilleurCout)
    print("Taille Pop "+str(len(generation.pop)))
    print("Best : "+str(generation.meilleurCout))
    res.append(resSim)
'''
'''

print("Res")
for sim in res :
    print("nbIndividusGarde "+str(sim[0])+" nbParents "+str(sim[1])+" nbInjection "+str(sim[2])+" %Mut "+str(sim[3])+" nbEnfants " + str(sim[4]) + " nbPivot "+ str(sim[5]))
    print("RES : "+str(sim[6]))
'''
'''
print(range(0,100))
print(res[0])
plt.plot(range(0,100),res[0],'r.',label="10%Parent")
plt.plot(range(0,100),res[1],'b.',label="15%Parent")
plt.plot(range(0,100),res[2],'g.',label="20%Parent")
plt.legend()
plt.show()
'''

res = []
#Lancement Auto de Paramètre Auto
for i in range(10) :
    generation = copy.deepcopy(genSave)
    generation.findCout()
    meilleurCout = 9999999
    resSim = []
    bestIndividusGarde = random.choice([5,10,15,20,25,30])
    nbParents = random.choice([5,10,15,20,25,30])
    nb_Injection = random.choice([0,5,10,15,20])
    pourcent_mutation = random.choice([0,5,10,15,20])
    nbEnfants = len(generation.pop)- nb_Injection
    nbPivot = random.choice([1,2,3,4,5])
    for i in range(100) :
        generation = generation.pivotMultipleConversation(bestIndividusGarde,nbParents,nbEnfants,nbPivot,villes)
        for indi in generation.pop :
            indi.mutation(pourcent_mutation)
        generation.injection(nb_Injection,villes)
        generation.findCout()
        if generation.meilleurCout < meilleurCout :
            meilleurCout = generation.meilleurCout
        resSim.append(generation.meilleurCout)
    res.append(resSim)
    plt.plot(range(0,100),resSim,label= "IndiG "+str(bestIndividusGarde)+" Parents "+str(nbParents)+" Inj "+str(nb_Injection)+" Mut "+ str(pourcent_mutation)+" Pivot "+str(nbPivot))
plt.legend()
plt.show()

